/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.t02.jsfCDI;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "classrooms")
@RequestScoped
public class Classrooms {
    private List<SelectItem> rooms;

    /**
     * Creates a new instance of Classrooms
     */
    public Classrooms() {
    }
    
    @PostConstruct
    private void init(){
        rooms = new ArrayList<>();
        rooms.add(new SelectItem(01, "陳"));
        rooms.add(new SelectItem(02, "王"));
        rooms.add(new SelectItem(03, "林"));
    }

    public List<SelectItem> getRooms() {
        return rooms;
    }

    public void setRooms(List<SelectItem> rooms) {
        this.rooms = rooms;
    }
    
    
    
}
