/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hychen39.t02.jsfCDI;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 * @since Nov 13, 2019
 */
@Named(value = "teacherFacade")
@RequestScoped
public class TeacherFacade {
    private List<SelectItem> allTeachers;
    
    @PostConstruct
    private void init(){
        allTeachers = new ArrayList<>();
        allTeachers.add(new SelectItem(1, "陳老師"));
        allTeachers.add(new SelectItem(2, "林老師"));
        allTeachers.add(new SelectItem(3, "王老師"));
    }
    public List<SelectItem> getAllTeachers() {
        return allTeachers;
    }

    public void setAllTeachers(List<SelectItem> allTeachers) {
        this.allTeachers = allTeachers;
    }
    
    
}
