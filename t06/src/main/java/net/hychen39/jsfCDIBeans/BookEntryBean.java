/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.jsfCDIBeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PostValidateEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;
import net.hychen39.entities.Book;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "bookEntryBean")
@SessionScoped
public class BookEntryBean implements Serializable, SystemEventListener {
    private Book book;
    private boolean isFormSubmitted;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public boolean isIsFormSubmitted() {
        return isFormSubmitted;
    }

    public void setIsFormSubmitted(boolean isFormSubmitted) {
        this.isFormSubmitted = isFormSubmitted;
    }
    
    
    /**
     * Creates a new instance of BookEntryBean
     */
    public BookEntryBean() {
    }
    
    @PostConstruct
    public void init(){
        book = new Book();
        this.isFormSubmitted = false;
    }


    @Override

    public void processEvent(SystemEvent se) throws AbortProcessingException {
        if ((se instanceof PostValidateEvent) && ( this.isFormSubmitted == false) ){
            this.isFormSubmitted = true;
        }
    }

    @Override
    public boolean isListenerForSource(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
