
t06 code slipts

# UI layout 雛型

```xml
<h:head>
        <title>訓練課程 6 表單驗證</title>
        <!--use bootstrap framework -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    </h:head>
    <h:body>
        <div class="container">
            <h1>訓練課程 6 表單驗證</h1>
            <a href="#"> 訓練課程 6 教學文件</a>
            <!-- Content here -->
            <h:form>
                <h2>課程資料</h2>
                <div class="form-group">
                    <h:outputLabel for="college">學院</h:outputLabel>
                    <h:inputText class="form-control" id="college"  />
                </div>
                <div class="form-group">
                    <h:outputLabel for="department">科系</h:outputLabel>
                    <h:inputText class="form-control" id="department"  />
                </div>
                <div class="form-group">
                    <h:outputLabel for="teacher">任課教師</h:outputLabel>
                    <h:inputText class="form-control" id="teacher"  />
                </div>
                <h2>書藉資料</h2>
                <div class="form-group">
                    <h:outputLabel for="book_title">書名</h:outputLabel>
                    <h:inputText class="form-control" id="book_title"  />
                </div>
                <div class="form-group">
                    <h:outputLabel for="isbn">ISBN</h:outputLabel>
                    <h:inputText class="form-control" id="isbn"  />
                </div>
                <div class="form-group">
                    <h:outputLabel for="sell_price">售價</h:outputLabel>
                    <h:inputText class="form-control" id="sell_price"  />
                </div>
                
                書況 (未套用 Bootstrap CSS)
               <h:selectOneRadio >
                   <f:selectItem itemLabel="A 級" itemValue="A" />
                   <f:selectItem itemLabel="B 級" itemValue="B" />
                   <f:selectItem itemLabel="C 級" itemValue="C" />
               </h:selectOneRadio>

                <h:commandButton class="btn btn-primary" value="Submit"></h:commandButton>
            </h:form>
        </div>
        <!--bootstrap js libraries-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </h:body>
```
