/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hychen39.t09.CDIBeans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "messageBean")
@RequestScoped
public class MessageBean{

    FacesContext facesContext;
    private String selectedTransportation;
    
    /**
     * Creates a new instance of MessageBean
     */
    public MessageBean() {
    }

    @PostConstruct
    public void init() {
        facesContext = FacesContext.getCurrentInstance();
    }

    public String getSelectedTransportation() {
        return selectedTransportation;
    }

    public void setSelectedTransportation(String selectedTransportation) {
        this.selectedTransportation = selectedTransportation;
    }

    public String showMessage() {
        String clientId = "";
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Info", "This is the information");
        facesContext.addMessage(clientId, facesMessage);
        return null;
    }

    
    public String showDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 640);
        options.put("height", 340);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");
        
        String outcome = "/dialogs/myDialog";
        
        Map<String, List<String>> params = new HashMap<>();
        
        params.put("dialog-title", Arrays.asList("Select your transportation"));
        params.put("dialog-message", Arrays.asList("Which one do you prefer?"));
        
        PrimeFaces.current().dialog().openDynamic(outcome, options, params);
        
        return null;
    }
    
    public String selectOption(String option){
        System.out.println("Close the current dynamic dialog");
        PrimeFaces.current().dialog().closeDynamic(option);
        return null;
    }
    
    public void onSelectTransportation(SelectEvent event){
        String value = (String) event.getObject();
        this.selectedTransportation = value;
    }
    
    public void showDialogMessage(String info){
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, info, info + " more details ...");
        PrimeFaces.current().dialog().showMessageDynamic(facesMessage);
    }
}
