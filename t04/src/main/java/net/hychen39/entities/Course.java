/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hychen39.entities;

/**
 * 課程資料模型物件
 * @author hychen39@gm.cyut.edu.tw
 * @since Nov 26, 2019
 */
public class Course {
    // primary key
    private int id;
    private String name;
    private String teacher;
    private String system;
    private String classID;
    private int weekday;
    private String sessions;
    private String classRoom;

    public Course() {
    }

    
    public Course(int id, String name, String teacher, String system, String classID, int weekday, String session, String classRoom) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
        this.system = system;
        this.classID = classID;
        this.weekday = weekday;
        this.sessions = session;
        this.classRoom = classRoom;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }
    
    
           
    
}
