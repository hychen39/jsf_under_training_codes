/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.dataAccess;

import java.util.List;
import net.hychen39.entities.Course;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
public class CourseRepositoryTest {
    
    private CourseRepository courseRepository;
    
    public CourseRepositoryTest() {
        courseRepository = new CourseRepository();
        courseRepository.init();
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    /**
     * Test of queryByRoomID method, of class CourseRepository.
     */
    @org.junit.jupiter.api.Test
    public void testQueryByRoomID() throws Exception {
        String roomID = "M-516";
        List<Course> results = this.courseRepository.queryByRoomID(roomID);
        assertNotNull(results);
        assertNotEquals(results.size(), 0);
        System.out.println(results.size());
    }
    
}
