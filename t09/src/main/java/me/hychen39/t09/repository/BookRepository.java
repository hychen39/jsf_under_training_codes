/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.hychen39.t09.repository;

import entities.Book;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Named;
import repository.AbstractRepository;


/**
 *
 * @author hychen39@gm.cyut.edu.tw
 * @since Mar 22, 2020
 */
@Named(value = "bookRepository")
@Singleton
public class BookRepository extends AbstractRepository<Book> {

    public BookRepository() {
        super(new HashMap<Integer, Book>());
    }
   
    @PostConstruct
    public void init(){
        Book book1 = new Book();
        book1.setBookName("Book1");
        book1.setStatus("New");
        this.add(book1);
        
        Book book2 = new Book();
        book2.setBookName("Book2");
        book2.setStatus("New");
        this.add(book2);
    }

}
