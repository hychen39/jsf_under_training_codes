/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.jsfCDIBeans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.model.SelectItem;

/**
 * 課表查詢的條件參數
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "classTableQueryParameters")
@SessionScoped
public class ClassTableQueryParameters implements Serializable{
    final static public String PROGRAM_UNDER = "under";
    final static public String PROGRAM_GRADUATE = "graduate";
    //學年度
    private int schoolYear; 
    //學制
    private String program;
    // 班級
    private int classCode;
    
    private Map<String, List<SelectItem>> classes;

    public int getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(int schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public int getClassCode() {
        return classCode;
    }

    public void setClassCode(int classCode) {
        this.classCode = classCode;
    }
    
    
    
    
    /**
     * Creates a new instance of ClassTableQueryParameters
     */
    public ClassTableQueryParameters() {
    }
    
    @PostConstruct
    public void init(){
        // init Property values
        this.schoolYear = 108;
        this.program = ClassTableQueryParameters.PROGRAM_UNDER;
        this.classCode = 1;
        
        // init the class
        classes = new HashMap<>();
        SelectItem under_classes[] = new SelectItem[]{
            new SelectItem(1, "A"), 
            new SelectItem(2, "B"), 
            new SelectItem(3, "C"),
        };
        classes.put(ClassTableQueryParameters.PROGRAM_UNDER, 
                Arrays.asList(under_classes));
        
        classes.put(ClassTableQueryParameters.PROGRAM_GRADUATE, 
                Arrays.asList(new SelectItem[]{
                    new SelectItem(4, "研一"),
                    new SelectItem(5, "研二"),
                }));
        
    }
    
    public String execQuery(){
        return "queryResults";
    }
    /**
     * 回傳特定學制下的班級清單
     * The bean scope should be ViewScoped at least. 
     * 否則子 selectOneMenu 會出現錯誤: Validation fails with the message "form:location: Validation Error: Value is not valid".
     * See https://stackoverflow.com/questions/9069379/validation-error-value-is-not-valid
     * @return 
     */
    public List<SelectItem> findClasses(){
        
        return this.classes.get(this.program);
       
    }
}
