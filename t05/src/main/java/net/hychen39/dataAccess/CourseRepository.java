/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hychen39.dataAccess;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import net.hychen39.entities.Course;

/**
 * 課程資料儲存庫, 用來模擬資料庫的存取功能。
 * @author hychen39@gm.cyut.edu.tw
 * @since Nov 26, 2019
 */

@Singleton
public class CourseRepository {
    private Map<Integer, Course> repository;
    
    @PostConstruct
    public void init(){
        repository = new Hashtable<>();
        // Put data
        repository.put(2654, new Course(2654, "商業英文", "劉熒潔", "四日資管組", "3C", 1 , "5,6", "T2-208" ));
        repository.put(2652, new Course(2652, "APP程式設計", "洪國龍", "四日資管組", "3C", 1 , "7,8", "M-516" ));
        repository.put(2632, new Course(2632, "Web程式設計", "唐元亮", "四日資管組", "2C", 5 , "5,7", "M-516" ));
    }
    
    /**
     * Create a course 
     * @param course 
     */
    public void create(Course course){
    }
    
    /**
     * Query a course by the primary key
     * @param courseID Primary Key of the course
     * @return 
     */
    public Course query(int courseID){
        if (repository.containsKey(courseID)){
            return repository.get(courseID);
        } else
            return null;
    }
    
    public List<Course> queryAll(){
        List<Course> allCourse = new ArrayList<>(repository.values());
        return allCourse;
    }
    
    public List<Course> queryByRoomID(String roomID){
        List<Course> allCourse = new ArrayList<>(repository.values());
        
       List<Course> results = allCourse.stream()
                .filter( (course) -> roomID.equals(course.getClassRoom()))
                .collect(Collectors.toList());
        return results;
    }

}
