/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.dataProcessing;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import net.hychen39.entities.Course;

/**
 * 週課表處理物件, 負責產生週課表所需要的資料。
 *
 * @author hychen39@gm.cyut.edu.tw
 * @since Dec 10, 2019
 */
public class WeekClassTableMaker implements ConvertToClassTableMap{

    /**
     * 依節次及週天為索引的課程資料
     */
    private Map<Integer, Map<Integer, Course>> classBySessionWeekday;

    public Map<Integer, Map<Integer, Course>> getClassBySessionWeekday() {
        return classBySessionWeekday;
    }

    public WeekClassTableMaker() {
        this.initTestData();
    }
    
    /**
     * 手動初始化資料, 將課程資料放入 {@link #classBySessionWeekday} 欄位中. 
     */
    private void initTestData() {
        classBySessionWeekday = new Hashtable<>();
        Course course1 = new Course(2654, "商業英文", "劉熒潔", "四日資管組", "3C", 1, "5,6", "T2-208");
        Course course2 = new Course(2652, "APP程式設計", "洪國龍", "四日資管組", "3C", 1, "7,8", "M-516");
        Course course3 = new Course(2632, "Web程式設計", "唐元亮", "四日資管組", "2C", 5, "5,6", "M-516");
        // 第5節, 各週天的課程. 
        Map<Integer, Course> weekday_session5 = new Hashtable<>();
        weekday_session5.put(1, course1);
        weekday_session5.put(5, course3);
        classBySessionWeekday.put(5, weekday_session5);
        // 第6節, 各週天的課程. 
        Map<Integer, Course> weekday_session6 = new Hashtable<>();
        weekday_session6.put(1, course1);
        weekday_session6.put(5, course3);
        classBySessionWeekday.put(6, weekday_session6);

        // 第7節, 各週天的課程. 
        Map<Integer, Course> weekday_session7 = new Hashtable<>();
        weekday_session7.put(1, course2);

        classBySessionWeekday.put(7, weekday_session7);

        // 第8節, 各週天的課程. 
        Map<Integer, Course> weekday_session8 = new Hashtable<>();
        weekday_session8.put(1, course2);
        classBySessionWeekday.put(8, weekday_session8);
    }

    /**
     * 設定產生課表所需要的資料. 外層的 Map 以 session 為 key 值, 取得該節次週一到週五的課程資料的 Map.
     * 內層的 Map 以 weekday (週天)為 key 值取得特定的課程資料。
     * @param classBySessionWeekday 
     */
    public void setClassBySessionWeekday(Map<Integer, Map<Integer, Course>> classBySessionWeekday) {
        this.classBySessionWeekday = classBySessionWeekday;
    }

    /**
     * 提供節次及週天, 回傳課程資料字串. 
     *
     * @param session 節次
     * @param weekday 週天
     * @return 課程資料字串或空字串。
     */
    public String findCourse(int session, int weekday) {
        StringBuilder result = new StringBuilder();
        if (this.classBySessionWeekday.containsKey(session)) {
            if (this.classBySessionWeekday.get(session).containsKey(weekday)) {
                Course course = this.classBySessionWeekday.get(session).get(weekday);
                result.append(course.getName())
                        .append(" ")
                        .append(course.getSystem())
                        .append(" ")
                        .append(course.getTeacher());
            }
        }
        return result.toString();
    }

    @Override
    public Map<Integer, Map<Integer, Course>> convert(List<Course> courses) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Map<Integer, Map<Integer, Course>> classTableData = new Hashtable<>();
        
        for(Course course: courses){
            int weekday = course.getWeekday();
            String [] sessions = course.getSessions().split(",");
            
            for (String sessionStr: sessions){
                int session = Integer.parseInt(sessionStr);
                // get the Weekday Map from the current session;
                if (classTableData.containsKey(session)){
                    classTableData.get(session).put(weekday, course);
                } else {
                    Map<Integer, Course> courseByWeekday = new Hashtable<>();
                    courseByWeekday.put(weekday, course);
                    classTableData.put(session, courseByWeekday);
                }
            }
        }
        
        return classTableData;
    }
    
    public static String convertToString(Course course){
        StringBuilder result = new StringBuilder();
         result.append(course.getName())
                        .append(" ")
                        .append(course.getSystem())
                        .append(" ")
                        .append(course.getTeacher());
         return result.toString();
    }
}
