/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.dataProcessing;

import java.util.List;
import java.util.Map;
import net.hychen39.entities.Course;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
public interface ConvertToClassTableMap {
    /**
     * 將 Course List 內的資料轉換成一個 2D 的 Map. 第一個維度為 session (節次) 第二個維度為 weekday (週天).
     * @param courses
     * @return 
     */
    Map<Integer, Map<Integer, Course>> convert(List<Course> courses);
}
