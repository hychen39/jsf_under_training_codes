/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.jsfCDIBeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import net.hychen39.dataProcessing.WeekClassTableMaker;
import net.hychen39.entities.Course;

/**
 * 存放週課表, 供顯示.
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "weekClassTableBean")
@SessionScoped
public class WeekClassTableBean implements Serializable {
    private Integer[] session = new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    private Integer[] weekDday = new Integer[]{1,2,3,4,5,6,7};
    private WeekClassTableMaker weekClassTableMaker; 
    /**
     * Creates a new instance of WeekClassTableBean
     */
    public WeekClassTableBean() {
    }

    @PostConstruct
    public void init() {
        weekClassTableMaker = new WeekClassTableMaker();
    }

    public Integer[] getSession() {
        return session;
    }

    public void setSession(Integer[] session) {
        this.session = session;
    }

    public Integer[] getWeekDday() {
        return weekDday;
    }

    public void setWeekDday(Integer[] weekDday) {
        this.weekDday = weekDday;
    }

    public String findCourse(int session, int weekday){
        return weekClassTableMaker.findCourse(session, weekday);
    }
    
  

}
