/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.jsfCDIBeans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.inject.Inject;
import net.hychen39.dataAccess.CourseRepository;
import net.hychen39.entities.Course;

/**
 * 課表查詢的條件參數
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "classTableQueryParameters")
@SessionScoped
public class ClassTableQueryParameters implements Serializable{
    // 教室
    private String classRoom;
    
    // 由 App Server 注入 Instance 給我們使用
     @Inject
    private CourseRepository courseRepository;
    // 存放執行查詢的結果 
    private List<Course> queryResults;

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    
    public List<Course> getQueryResults() {
        return queryResults;
    }

    public void setQueryResults(List<Course> queryResults) {
        this.queryResults = queryResults;
    }

    
    
    
    
    /**
     * Creates a new instance of ClassTableQueryParameters
     */
    public ClassTableQueryParameters() {
    }
    
    @PostConstruct
    public void init(){
      
        // 初始化欄位值
        this.classRoom = "M-516";
    }
    
    /**
     * 執行查詢的 Action Method
     * 查詢結果存回 {@link #queryResults}。
     * @return "index"
     */
    public String execQuery(){
        // execute query
        this.queryResults = courseRepository.queryByRoomID(this.classRoom);
        return "index";
    }
    
}
