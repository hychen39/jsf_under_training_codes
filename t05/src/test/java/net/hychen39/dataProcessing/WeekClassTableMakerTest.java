/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.hychen39.dataProcessing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.hychen39.entities.Course;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
public class WeekClassTableMakerTest {
    
    public WeekClassTableMakerTest() {
    }

    /**
     * Test of convert method, of class WeekClassTableMaker.
     */
    @Test
    public void testConvert() {
        List<Course> courses = new ArrayList<>();
        Course course1 = new Course(2654, "商業英文", "劉熒潔", "四日資管組", "3C", 1, "5,6", "T2-208");
        Course course2 = new Course(2652, "APP程式設計", "洪國龍", "四日資管組", "3C", 1, "7,8", "M-516");
        Course course3 = new Course(2632, "Web程式設計", "唐元亮", "四日資管組", "2C", 5, "5,6", "M-516");
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        
        Map<Integer, Map<Integer, Course>> classTableData = null;
        ConvertToClassTableMap classTableConverter = new WeekClassTableMaker();
        classTableData = classTableConverter.convert(courses);
        assertEquals(classTableData.size(), 4);
        System.out.printf("Map size: %d \n", classTableData.size() );
        // print data
        classTableData.forEach((session, courseByWeekday) -> {
            courseByWeekday.forEach((weekday, course) -> {
                System.out.printf("Session: %d, Weekday %d, %s \n", session, weekday, WeekClassTableMaker.convertToString(course));
            });
        });
    }

    /**
     * Test of getClassBySessionWeekday method, of class WeekClassTableMaker.
     */
    @Test
    public void testGetClassBySessionWeekday() {
        assertEquals(1, 0);
    }

    /**
     * Test of setClassBySessionWeekday method, of class WeekClassTableMaker.
     */
    @Test
    public void testSetClassBySessionWeekday() {
    }

    /**
     * Test of findCourse method, of class WeekClassTableMaker.
     */
    @Test
    public void testFindCourse() {
    }

    /**
     * Test of convertToString method, of class WeekClassTableMaker.
     */
    @Test
    public void testConvertToString() {
    }
    
}
