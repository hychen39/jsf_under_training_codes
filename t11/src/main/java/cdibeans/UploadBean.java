package cdibeans;

import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

@Named(value = "uploadBean")
@SessionScoped
// Use lombok to generate the getters and setters. See: https://kucw.github.io/blog/2020/3/java-lombok/ 
@Getter
@Setter
public class UploadBean implements Serializable {

    private Part uploadPart;
    private String filename = "";
    private String base64Photo;

    /**
     * Save file to the file system in the server-side.
     *
     * @return null (Stay in the same page).
     */
    public String uploadAction() {
        if (uploadPart == null) {
            return null;
        }

        //Get the input string from the Part Object
        InputStream is = null;
        try {
            is = uploadPart.getInputStream();
            // Convert the input stream to the byte array
            // Ref: https://magiclen.org/java-base64/
            byte[] bytes = is.readAllBytes();
            // Convert the base64 string to data uri
            // Ref: https://blog.gtwang.org/web-development/minimizing-http-request-using-data-uri/
            // Schema:  data:[<media type>][;base64],<data>
            String base64Str = Base64.getEncoder().encodeToString(bytes);
            this.base64Photo = "data:image/png;base64," + base64Str;
        } catch (IOException ex) {
            Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Set the file name property
        filename = uploadPart.getSubmittedFileName();

        return null; // Stay in the same page
    }

    /**
     * Handler for p:fileUpload
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {
        // UploadedFile api: https://www.primefaces.org/docs/api/8.0/
        UploadedFile file = event.getFile();
        //application code
        // ToDo: Convert to base64 string
        byte[] content = file.getContent();
        String base64Content = Base64.getEncoder().encodeToString(content);
        System.out.println("base64Content: " + base64Content);
        this.base64Photo = "data:image/png;base64," + base64Content;
    }

    /**
     * Ref: https://stackoverflow.com/questions/16336795/jsf-2-2-fileupload-does-not-work-with-ajax-form-appears-to-have-incorrect-enc
     * @param event 
     */
    public void handleFileUploadAjaxListener(AjaxBehaviorEvent event) {
        System.out.println("file size: " + uploadPart.getSize());
        System.out.println("file type: " + uploadPart.getContentType());
        System.out.println("file info: " + uploadPart.getHeader("Content-Disposition"));
        this.base64Photo = toBase64Str(uploadPart);
        this.filename = uploadPart.getSubmittedFileName();
    }
    
    private String toBase64Str(Part part){
        String result ="";
        try {
            InputStream is = uploadPart.getInputStream();
            // Convert the input stream to the byte array
            // Ref: https://magiclen.org/java-base64/
            byte[] bytes = is.readAllBytes();
            // Convert the base64 string to data uri
            // Ref: https://blog.gtwang.org/web-development/minimizing-http-request-using-data-uri/
            // Schema:  data:[<media type>][;base64],<data>
            String base64Str = Base64.getEncoder().encodeToString(bytes);
            result = "data:image/png;base64," + base64Str;
        } catch (IOException ex) {
            Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
